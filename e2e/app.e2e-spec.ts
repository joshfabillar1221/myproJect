import { SrMainPage } from './app.po';

describe('sr-main App', () => {
  let page: SrMainPage;

  beforeEach(() => {
    page = new SrMainPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
