import { Component, OnInit } from '@angular/core';
import { SrusersService } from '../srusers.service';
import { AuthenticationService } from '../services/index';
import { srusersModel } from '../models/srusers';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  srusers = new srusersModel;
  usersVO:srusersModel;
  errorMessage: string;
  returnUrl: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,

  ) {}

  ngOnInit() {
      this.authenticationService.Logout();
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  public doLogin() {
    this.authenticationService.LoginAuthentication(this.srusers)
      .subscribe(
        data => {
          // this.usersVO = datae
          this.errorMessage = null;
          if(data == 100) {
            this.router.navigate(['/']);
          }
        },
        error => this.errorMessage = error.json().message
      );
  }

  private testVO() {
    console.log(this.usersVO);
  }

  private errorLog(error) {
    console.log(error);
  }

}
