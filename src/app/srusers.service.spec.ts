import { TestBed, inject } from '@angular/core/testing';

import { SrusersService } from './srusers.service';

describe('SrusersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SrusersService]
    });
  });

  it('should ...', inject([SrusersService], (service: SrusersService) => {
    expect(service).toBeTruthy();
  }));
});
