import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard }            from './guard/auth.guard';
import { LoginComponent }       from './login/login.component';
import { ShellComponent }       from './shell/shell.component';
import { NavigationComponent }  from './navigation/navigation.component';
import { HeaderComponent }      from './header/header.component';
import { ContentComponent }     from './content/content.component';
import { ProfileComponent }     from './profile/profile.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent, pathMatch: 'full' },
    { path: '', redirectTo: '/profile', pathMatch: 'full' },
    {
      path: '',
      component: ShellComponent,
      canActivate: [ AuthGuard ],
      children: [
        { path: 'profile', component: ProfileComponent, data: { title: 'Home' } },
        { path: 'navigation', component: NavigationComponent, data: { title: 'Navigation' } },
        { path: 'header', component: HeaderComponent, data: { title: 'Header' } },
        { path: 'content', component: ContentComponent, data: { title: 'Content' } }
      ]
    },
    { path: '**', redirectTo: '/profile', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
