import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response   } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

  constructor( private http: Http ) { }

  private headers = new Headers({'Content-Type': 'application/json'});
  private options = new RequestOptions({ headers: this.headers });
  private apiUrl = 'http://localhost:3000/';

  LoginAuthentication(user) {
    return this.http.post(this.apiUrl + 'api/sr/validate',
      JSON.stringify(user), this.options)
        .map((response: Response) => {
            let user = response.json().result;
            // save currentUser and cookies on local storage
            localStorage.setItem('currentUser', JSON.stringify(user));
            return user.usertype;

        });
  }

  Logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
  }

}
