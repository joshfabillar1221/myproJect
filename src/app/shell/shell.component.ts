import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent implements OnInit {

  email = String;
  constructor() { }

  ngOnInit() {
    this.email = JSON.parse(localStorage.getItem('currentUser')).email;
  }

}
